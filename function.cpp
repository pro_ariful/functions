/* Function Prototype */

/*In C++, the code of function declaration should be before the function call. 
However, if we want to define a function after the function call, 
we need to use the function prototype */

#include <iostream>

using namespace std;

// function prototype

int add (int, int);

// main function
int main(){
	// calling the function and storing
    // the returned value in sum
	int sum;
	sum = add (100,78);
	
	cout << "100 + 78 = " << sum <<endl;


}

// function definition

int add(int a, int b){
	return (a+b);
}
